const weather = require('./models/weather');
const mongoDb = require('./context/mongoContext');

module.exports.insertCovilha = async () => {
  const temp = await weather.getCurrTemp('2740313'); // cityId of Covilha, Portugal
  const document = await mongoDb.insert('covilha', { temp, ts: Date.now() }); // insert in mongodb
  return document;
};

module.exports.currTempCovilha = async () => {
  const response = {
    statusCode: 200,
    // retrieve last record from mongodb
    body: JSON.stringify(await mongoDb.query('covilha', { sort: { _id: -1 } })), 
  };
  return response;
};

module.exports.avgTempSfax = async () => {
  const response = {
    statusCode: 200,
    // coordinates of Sfax, Tunis
    body: JSON.stringify(await weather.getAvgTemp({ lat: 34.733333, lon: 10.75 })),
  };
  return response;
};
