const openWeather = require('../context/openWeatherContext');
const meteoStat = require('../context/meteoStatContext');

const getClimate = async (coords) => meteoStat.getData(`/v2/point/climate?lat=${coords.lat}&lon=${coords.lon}`);

const getCityData = async (cityId) => openWeather.getData(`weather?id=${cityId}`);

module.exports.getCurrTemp = async (cityId) => getCityData(cityId)
  .then((result) => result.main.temp);

module.exports.getAvgTemp = async (coords) => getClimate(coords)
  .then((result) => result.data[5].tavg);
