const { MongoClient } = require('mongodb');

const { MONGODB_URI } = process.env;
module.exports.query = function (collection, query) {
  console.log('=> query database');
  return MongoClient.connect(MONGODB_URI).then((client) => {
    const db = client.db(collection);
    return db.collection(collection).findOne(
      {},
      query,
    );
  });
};

module.exports.insert = function (collection, data) {
  console.log('=> insert database');
  return MongoClient.connect(MONGODB_URI).then((client) => {
    const db = client.db(collection);
    return db.collection(collection).insertOne(data).then((document) => document.ops[0]);
  });
};
