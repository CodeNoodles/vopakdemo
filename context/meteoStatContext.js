const https = require('https');

module.exports.getData = async (params) => new Promise((resolve, reject) => {
  let dataString = '';
  const req = https.get({
    host: 'api.meteostat.net',
    path: `${params}`,
    type: 'application/json',
  },
  (res) => {
    res.on('data', (chunk) => {
      dataString += chunk;
    });

    res.on('end', () => {
      resolve(JSON.parse(dataString), null, 4);
    });
  });

  req.on('error', (e) => {
    reject(new Error(`Something went wrong: 
                ${e}`));
  });
});
