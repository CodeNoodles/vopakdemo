const https = require('https');

const apiKey = 'ae4d21491138ce4d49621b133fb0169b';
module.exports.getData = async (params) => new Promise((resolve, reject) => {
  let dataString = '';
  const req = https.get(`https://api.openweathermap.org/data/2.5/${params}&appid=${apiKey}&units=metric`, (res) => {
    res.on('data', (chunk) => {
      dataString += chunk;
    });
    res.on('end', () => {
      resolve(JSON.parse(dataString), null, 4);
    });
  });

  req.on('error', (e) => {
    reject(new Error(`Something went wrong: 
                ${e}`));
  });
});
