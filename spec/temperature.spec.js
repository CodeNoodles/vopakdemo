/* eslint-disable */
const weather = require('../models/weather.js');

describe('GetCurrTemp', () => {
  it('return current temperature of Covilha', async () => {
    const result = await weather.getCurrTemp('2740313'); // City Id of Covilha
    // If we get any number we conclude we are getting a temp from the api
    expect(result).toEqual(jasmine.any(Number));
  });
});

describe('GetAvgTemp', () => {
  it('return avg temp of Sfax in june', async () => {
    // coordinates of Sfax
    const result = await weather.getAvgTemp({ lat: 34.733333, lon: 10.75 });
    expect(result).toEqual(jasmine.any(Number));
  });
});
